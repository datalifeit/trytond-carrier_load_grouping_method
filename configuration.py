# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelSQL, ValueMixin
from trytond.pool import PoolMeta

load_grouping_method = fields.Selection([
        ('standard', 'Standard'),
        ('product_price', 'Product & price'),
        ], 'Load Grouping Method', required=True)


class Configuration(metaclass=PoolMeta):
    __name__ = 'party.configuration'

    load_grouping_method = fields.MultiValue(load_grouping_method)

    @classmethod
    def default_load_grouping_method(cls, **pattern):
        return cls.multivalue_model('load_grouping_method'
            ).default_load_grouping_method()


class ConfigurationLoad(ModelSQL, ValueMixin):
    """Party Configuration Load Ul Grouping Method"""
    __name__ = 'party.configuration.load_grouping_method'

    load_grouping_method = load_grouping_method

    @classmethod
    def default_load_grouping_method(cls):
        return 'standard'

# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    load_grouping_method = fields.Selection([
        ('standard', 'Standard'),
        ('product_price', 'Product & price'),
        ], 'Load Grouping Method')

    @classmethod
    def default_load_grouping_method(cls, **pattern):
        Configuration = Pool().get('party.configuration')
        config = Configuration(1)
        load_grouping_method = config.get_multivalue(
            'load_grouping_method', **pattern)
        return load_grouping_method if load_grouping_method else None
